use std::fs;

/// Part 1
fn calc_fuel(module: u32) -> Option<u32> {
    let module: f32 = ((module / 3) as f32).floor();
    (module as u32).checked_sub(2)
}

/// Part 2
fn calc_total_fuel(mut module: u32) -> u32 {
    let mut total = 0;
    loop {
        if let Some(fuel) = calc_fuel(module) {
            total += fuel;
            module = fuel;
        } else {
            break
        }
    }

    total
}

fn main() {
    let input = fs::read_to_string("one/input.txt").unwrap();
    let mut part_one = 0;
    let mut part_two = 0;

    for module in input.lines().map(|s| s.parse().unwrap()) {
        part_one += calc_fuel(module).unwrap();
        part_two += calc_total_fuel(module);
    }

    println!("Part 1 total: {}", part_one);
    println!("Part 2 total: {}", part_two);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calc_fuel() {
        assert_eq!(calc_fuel(12).unwrap(), 2);
        assert_eq!(calc_fuel(14).unwrap(), 2);
        assert_eq!(calc_fuel(1969).unwrap(), 654);
        assert_eq!(calc_fuel(100756).unwrap(), 33583);
        assert_eq!(calc_fuel(2).is_none(), true)
    }

    #[test]
    fn test_calc_total_fuel() {
        assert_eq!(calc_total_fuel(1969), 966);
        assert_eq!(calc_total_fuel(100756), 50346);
    }
}
