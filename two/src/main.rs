use std::fs;

fn add(intcode: &mut Vec<usize>, a: usize, b: usize, out: usize) {
    intcode[out] = intcode.get(a).unwrap() + intcode.get(b).unwrap();
}

fn mul(intcode: &mut Vec<usize>, a: usize, b: usize, out: usize) {
    intcode[out] = intcode.get(a).unwrap() * intcode.get(b).unwrap();
}

fn exec(mut intcode: Vec<usize>) -> Result<Vec<usize>, &'static str> {
    for i in (0..intcode.len()).step_by(4) {
        let a: usize = *intcode.get(i + 1).unwrap_or(&0);
        let b: usize = *intcode.get(i + 2).unwrap_or(&0);
        let out: usize = *intcode.get(i + 3).unwrap_or(&0);
        match intcode[i] {
            1 => add(&mut intcode, a, b, out),
            2 => mul(&mut intcode, a, b, out),
            99 => break,
            _ => return Err("Invalid intcode operator")
        }
    }

    Ok(intcode)
}

fn reverse_engineer(intcode: &mut std::vec::Vec<usize>, first_num: usize) -> Result<[usize; 2],  &'static str> {
    for a in 0..100 {
        for b in 0..100 {
            intcode[1] = a;
            intcode[2] = b;

            if exec(intcode.to_vec()).unwrap()[0] == first_num {
                return Ok([a, b])
            }
        }
    }

    Err("Unable to reverse engineer")
}

fn main() {
    let input = fs::read_to_string("two/input.txt").unwrap();
    let mut intcode: Vec<usize> = input.split(",").map(|s| s.trim().parse().unwrap()).collect();

    let part_two = reverse_engineer(&mut intcode, 19690720).unwrap();
    println!("Noun: {} Verb: {}", part_two[0], part_two[1]);
    println!("First value: {}", exec(intcode).unwrap()[0]);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add() {
        assert_eq!(exec(vec!(1,0,0,0,99, 0)).unwrap()[0], 2)
    }

    #[test]
    fn test_mul() {
        assert_eq!(exec(vec!(2,3,0,3,99, 0)).unwrap()[3], 6)
    }

    #[test]
    fn test_exec() {
        assert_eq!(exec(vec!(1,9,10,3,2,3,11,0,99,30,40,50)).unwrap()[0], 3500);
        assert_eq!(exec(vec!(3,3,0,3,99)).is_err(), true)
    }
}
