fn has_adjacent(n: usize) -> bool {
    let mut n: Vec<char> = n.to_string().chars().collect();
    n.dedup();
    n.len() < 6
}

fn not_part_of_larger_adjacent(n: usize) -> bool {
    let n: Vec<char> = n.to_string().chars().collect();
    let mut nums: Vec<Vec<char>> = vec!(vec!());
    for c in n {
        let i = nums.len() - 1;

        if let Some(v) = nums[i].first() {
            if v == &c {
                nums[i].push(c)
            } else {
                nums.push(vec!(c))
            }
        } else {
            // First number
            nums[i].push(c)
        }
    }

    nums.iter().filter(|v| v.len() == 2).collect::<Vec<&Vec<char>>>().len() >= 1
}

fn never_decreases(n: usize) -> bool {
    let s = n.to_string();
    let mut last = '0';
    for c in s.chars() {
        if c >= last {
            last = c
        } else {
            return false
        }
    }
    true
}

fn main() {

    let mut possibles1: Vec<usize> = vec!();
    let mut possibles2: Vec<usize> = vec!();

    for n in 136818..685979 {
        if has_adjacent(n) && never_decreases(n) {
            possibles1.push(n)
        }
        if not_part_of_larger_adjacent(n) && never_decreases(n) {
            possibles2.push(n)
        }
    }

    println!("Amount of possible numbers for part 1: {}", possibles1.len());
    println!("Amount of possible numbers for part 2: {}", possibles2.len());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_has_adjacent() {
        assert!(has_adjacent(122345));
        assert!(!has_adjacent(123789));
    }

    #[test]
    fn test_never_decrases() {
        assert!(never_decreases(123789));
        assert!(!never_decreases(223450));
        assert!(never_decreases(111123))
    }

    #[test]
    fn test_not_part_of_larger_adjacent() {
        assert!(not_part_of_larger_adjacent(112233));
        assert!(!not_part_of_larger_adjacent(123444));
        assert!(not_part_of_larger_adjacent(111122));
        assert!(!not_part_of_larger_adjacent(144444));
        assert!(!not_part_of_larger_adjacent(137777))
    }
}
